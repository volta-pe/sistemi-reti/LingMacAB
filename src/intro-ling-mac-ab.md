### Istruzioni del Linguaggio Macchina
Sia addr l'indirizzo di memoria, con [addr] si indica il valore in esso contenuto.
Con A si indica il registro A e con [A] il valore in esso contenuto.
Idem per B.

| cod.op. | istruzione | descrizione |
|---------|------------|-------------|
0000 | LOADA addr | Copia [addr] in A
0001 | LOADB addr | Copia [addr] in B
0010 | STOREA addr | Copia [A] in addr
0011 | STOREB addr | Copia [B] in addr
0100 | READ addr | Copia in addr il valore dell'input corrente e sposta la testina dell'input al prossimo elemento
0101 | WRITE addr | stampa [addr] in output
0110 | ADD | [A] + [B] -> A
0111 | DIF | [A] - [B] -> A
1000 | MUL | [A] * [B] -> A
1001 | DIV | [A] / [B] -> A resto B
1010 | JUMP addr | salto incondizionato ad addr
1011 | JUMPZ addr | salto condizionato ad addr se A è 0
1100 | NOP
1101 | HALT