## Somma di 2 numeri

Presi 2 valori dall'input, stampare la somma

**Soluzione :**

Sia 20 l'indirizzo dedicato a contenere i valori letti dall'input di volta in volta.
Sia 21 l'indirizzo dedicato a contenere la somma dei valori trovati.

```
100 READ 20
110 LOADA 20
120 READ 20
130 LOADB 20
140 ADD
150 STOREA 21
160 WRITE 21
170 HLT
```

<p style="page-break-before: always;"> </p>

## Somma di una lista di numeri

Dati una lista di valori in input, stampare la loro somma. La lista viene conclusa sempre da uno zero.

**Soluzione :**

Sia 20 l'indirizzo dedicato a contenere i valori letti dall'input di volta in volta.
Sia 21 l'indirizzo dedicato a contenere la somma dei valori trovati. Viene inizializzato a 0.

```
100 READ 20
101 LOADA 20
102 JMPZ 106
104 LOADB 21
103 ADD
104 WRITE 21
105 JMP 100
106 WRITE 21
107 HLT
```

<p style="page-break-before: always;"> </p>

## Massimo tra 2 numeri

Dati 2 numeri in input, stampare il massimo

**Soluzione :**

- In 22 viene memorizzato il primo valore
- In 23 viene memorizzato il secondo valore
- Dopo la divisione il maggiore dei due viene messo nel registro A
- In 20 viene copiato il valore di A
- Viene stampato il contenuto di 20

```
100 READ 22
101 LOADA 22
102 READ 23
103 LOADB 23
104 DIV
```

Se la divisione ha dato quoziente 0 (in A) allora in 23 c'è i valore maggiore ... salta a 108

```
105 JMPZ 108
```

Altrimenti il valore maggiore è quello presente in 22

```
106 LOADA 22
107 JMP 109
108 LOADA 23
109 STOREA 20
110 WRITE 20
111 HLT
```

<p style="page-break-before: always;"> </p>

## Massimo tra di una serie di numeri

Dati una lista di numeri in input (terminati da un 0), stampare il massimo

**Soluzione :**

Sia 20 il massimo corrente. Viene inzializzato con il primo valore trovato.
Sia 22 l'indirizzo dove viene memorizzato il numero corrente.

```
100 READ 22
101 LOADA 22
102 STOREA 20
```

Una volta impostato il primo valore come massimo ... viene letto il secondo valore

```
103 READ 22
104 LOADA 22
```

Se è 0 allora il calcolo è finito ... si passa alla stampa

```
105 JMPZ 112
```

Altrimenti si mette in B il massimo corrente e si divide

```
106 LOADB 20
107 DIV
```

Se il quoziente (in A) è 0 ... allora il massimo corrente vince ... si legge il prossimo numero

```
108 JMPZ 103
```

Altrimenti occorre salvare il valore corrente in 20 e leggere il numero successivo

```
109 LOADA 22
110 STOREA 20
111 JMP 103
```

Quando si legge uno 0 ... allora si stampa il massimo trovato

```
112 WRITE 20
113 HLT
```

<p style="page-break-before: always;"> </p>

## Somma dei numeri pari di una lista di numeri

Dati una lista di valori in input, sommare solo i valori pari e stampare il risultato.

**Soluzione :**

Sia 20 l'indirizzo dedicato a contenere i valori letti dall'input di volta in volta.
Sia 21 l'indirizzo dedicato a contenere la somma dei valori trovati. Viene inizializzato a 0.
Sia 22 l'indirizzo che contiene il valore 2.
Sia 23 l'indirizzo che contiene il resto della divisione per 2 del numero corrente.

```
100 READ 20
101 LOADA 20
```

Se il valore letto è zero si salta alla stampa

```
102 JMPZ 114
```

Il valore letto è diverso da zero ... allora si divide per 2 ...

```
103 LOADB 22
104 DIV
105 STOREB 23
106 LOADA 23
```

e si trasferisce il resto (presente in B) nel registro A passando per 23

```
107 JMPZ 109
```

Se è pari (cioè 0) si passa sommare il contenuto di 22 con quello di 21 ..

```
108 JMP 100
```

altrimenti si torna all'inizio per legge il valore successivo

```
109 LOADA 20
110 LOADB 21
111 ADD
```

Fatta la somma si memorizza in 21 e si torna all'inizio

```
112 STOREA 21
113 JMP 100
```

E' stato letto uno zero dall'input ... allora si stampa il risultato

```
114 WRITE 21
115 HLT
```
